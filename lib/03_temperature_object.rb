class Temperature
  attr_accessor :celsius, :fahrenheit
  def initialize(temp)
    if temp[:f]
      @fahrenheit = temp[:f]
    else
      @celsius = temp[:c]
    end
  end

  def in_fahrenheit
    @celsius ? @celsius * 9 / 5.0 + 32 : @fahrenheit
  end

  def in_celsius
    @fahrenheit ? (@fahrenheit - 32) * 5 / 9.0 : @celsius
  end

  def self.from_fahrenheit(temp)
    Temperature.new(:f => temp)
  end

  def self.from_celsius(temp)
    Temperature.new(:c => temp)
  end
end

class Celsius < Temperature
  def initialize(temp)
    self.celsius = temp
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    self.fahrenheit = temp
  end
end
