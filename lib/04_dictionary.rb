class Dictionary
  attr_accessor :entries

  def initialize
    @entries = {}
    @keywords = []
  end

  def add(entry)
    if entry.is_a?(String)
      @entries[entry] = nil
    else
      @entries.merge!(entry)
    end
  end

  def keywords
    @entries.map {|key, value| key}.sort
  end

  def include?(entry)
    @entries.include?(entry)
  end

  def find(word)
    entry = @entries.select {|key, value| key.include?(word)}
  end

  def printable
    self.keywords.map do |key|
      "[#{key}] \"#{self.entries[key]}\""
    end.join("\n")
  end
end
