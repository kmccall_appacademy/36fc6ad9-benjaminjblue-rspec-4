class Book
  attr_accessor :title
  def title=(title)
    title.capitalize!
    lowers = ["a", "an", "and", "of", "the", "to", "in"]
    @title = title.split.map do |word|
      lowers.include?(word) ? word : word.capitalize
    end.join(" ")
  end
end
