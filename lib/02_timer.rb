class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    hours = @seconds / 3600
    minutes = (@seconds - hours * 3600) / 60
    seconds = @seconds % 60
    "#{two_digit(hours)}:#{two_digit(minutes)}:#{two_digit(seconds)}"
  end

  def two_digit(int)
    int < 10 ? "0" + int.to_s : int.to_s
  end

end
